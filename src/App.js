/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import Web3 from "web3";
import "./App.css";
import { ABI_FILE, SMART_CONTRACT } from "./utils/config";

class App extends Component {
  async componentDidMount() {
    await this.loadWeb3();
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.enable();
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      );
    }
  }

  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:8545");
    await web3.eth.requestAccounts().then(console.log);
    const accounts = await web3.eth.getAccounts();
    const mainData = new web3.eth.Contract(ABI_FILE, SMART_CONTRACT);
    this.setState({ account: accounts[0] });
    this.setState({ toggle: false });
    const totalSupply = await mainData.methods.totalSupply().call();
    console.log("vijay_call", totalSupply);
    this.setState({ totalSupply });
  }

  mint = async (data) => {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:8545");
    const mainData = new web3.eth.Contract(ABI_FILE, SMART_CONTRACT);
    const price = await mainData.methods.chruchPrice().call();
    console.log(price);
    const mintPrice = price * data;
    var toWie = web3.utils.toWei(web3.utils.toBN(mintPrice), "wei");
    console.log("vijay_call_toWie", toWie);
    if (!data) {
      alert("please enter data");
    } else {
      mainData.methods
        .mintChruch(data)
        .send({
          from: this.state.account,
          value: toWie,
          maxPriorityFeePerGas: null,
          maxFeePerGas: null,
        })
        .once("receipt", async (receipt) => {
          console.log("vijay_call_receipt", receipt);
          await this.loadBlockchainData();
          this.setState({ status: true });
        });
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      data: "",
      account: "",
      toggle: true,
      status: false,
      totalSupply: "",
    };
    this.loadBlockchainData = this.loadBlockchainData.bind(this);
  }

  render() {
    return (
      <div className="bgWrapper">
        <div className="bannerRow">
          <div className="bannerImages">
            
          </div>
          <div className="bannerForm">
            <div className="formWrapper">
              <div className="topSection">
                <h3>
                  Total minted <span>{this.state.totalSupply}/10000</span>
                </h3>
                {this.state.toggle ? (
                  <button
                    className="btn mainButton"
                    onClick={this.loadBlockchainData}
                  >
                    Connect with metamask
                  </button>
                ) : (
                  <button className="btn mainButton">
                    Connected with metamask accounts : {this.state.account}
                  </button>
                )}
              </div>
              <div className="bottomSection">
                <h2>
                  MANY HOGSYA WANT? <span>(MAX 20)</span>
                </h2>
                <div className="inputitem">
                  <form
                    onSubmit={(event) => {
                      event.preventDefault();
                      const data = this.data.value;
                      this.mint(data);
                    }}
                  >
                    <input
                      className="inputItem"
                      ref={(input) => {
                        this.data = input;
                      }}
                      value={this.state.data}
                      onChange={(e) => this.setState({ data: e.target.value })}
                    />
                    <div className="inputButtonRow">
                      <input
                        className="btn mainButton"
                        type="submit"
                        value="MINT"
                        onClick={this.showMsg}
                        disabled={!this.state.account}
                      />
                      <input
                        className="btn mainButton"
                        onClick={() => this.setState({ data: 5 })}
                        type="submit"
                        value="mint Max"
                        disabled={!this.state.account}
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div className="bannerImages">

          </div>
          {this.state.status ? (
            <div>
              <div className="modalfade"></div>
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                      Welcome to the Horny Hogs Club! Please check your minting
                      transaction was successful on MetaMask
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                      onClick={() => this.setState({ status: false })}
                    >
                      <span aria-hidden="true">Ok</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default App;
